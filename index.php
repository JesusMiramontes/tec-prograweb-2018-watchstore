<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("./header.php"); ?>
</head>

<body class="bg-light" id="page-top">

<!-- Navigation -->
<?php include("nav.php")?>

<!-- Video -->
<div class="container" >
    <div class="row">
        <video autoplay class="" id="home-video" preload="auto" style="position: absolute; top: 55% !important; left: 50% !important; transform: translateX(-50%) translateY(-50%); width: 2164.36px; height: 868px;">
            <source src="./media/videos/toric-hemispheres.mp4" type="video/mp4">
        </video>
    </div>
</div>

<!-- Intro Header -->
<header class="masthead" style="margin-top: 36%; background:url(./media/images/watchset.jpg) no-repeat bottom center scroll;margin-bottom: 00px !important;">
    <div class="intro-body">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h1 class="brand-heading">Before time</h1>
                    <p class="intro-text">Antes del tiempo</p>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Footer -->
<?php include("footer.php")?>

</body>

</html>
