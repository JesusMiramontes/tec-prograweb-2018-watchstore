<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-shrink" id="mainNav" ">
    <div class="container" style="height: 55px !important;">
        <a class="navbar-brand js-scroll-trigger" href="./index.php">Before time</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto" >
                <li class="nav-item" style="color: black ;">
                    <a class="nav-link js-scroll-trigger" href="./store.php">Our products</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="./orders.php">My orders</a>
                </li>
                <li class="nav-item" style="padding-top: 8px; padding-left: 7px">
                    <a href="cart.php"><i class="fas fa-shopping-cart"></i></a>
                </li>
            </ul>
        </div>
    </div>
</nav>