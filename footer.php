<!-- Footer -->
<footer>
    <div class="container text-center">
        <p>Copyright &copy; Before time 2018; By Jesús Miramontes & Iván Sifuentes</p>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="./media/js/jquery/jquery.min.js"></script>
<script src="./media/js/bootstrap/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="./media/js/jquery-easing/jquery.easing.min.js"></script>

<!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

<!-- Custom scripts for this template -->
<script src="./media/js/grayscale/grayscale.min.js"></script>