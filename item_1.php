<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("./header.php"); ?>
</head>

<body class="bg-light" id="page-top">
<!-- Navigation -->
<?php include("nav.php")?>

<?php include("./banners/3.php")?>

<!-- Intro Header -->
<header class="masthead" style="margin-top: 0%; ">
    <div class="intro-body">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h1 class="brand-heading">Before time</h1>
                    <p class="intro-text">Ressence type 1</p>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="intro-body" style="margin-top: 7%">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="col-lg-12">

                    <div class="card mt-8">
                        <img class="card-img-top img-fluid" style="width: 100% !important; height: 100% !important;" src="./media/images/ressence/1.png"
                             alt="">
                        <div class="card-body">
                            <h3 class="card-title">Product Name</h3>
                            <h4>$24.99</h4>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente dicta fugit fugiat hic
                                aliquam itaque facere, soluta. Totam id dolores, sint aperiam sequi pariatur praesentium
                                animi perspiciatis molestias iure, ducimus!</p>
                            <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
                            4.0 stars
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </div>
</div>

<br>
<br>

<?include("footer.php")?>
</body>

</html>